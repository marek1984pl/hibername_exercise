/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.sda.standalone.hibernate_exercise;

import java.util.Date;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import pl.sda.standalone.hibernate_exercise.model.Vehicle;

/**
 *
 * @author RENT
 */
public class Main2 {
    public static void main(String[] args) { 
            
        SessionFactory instance = HibernateManager.getInstance();
        Session session = instance.openSession();
        Transaction tx = session.beginTransaction();
        
        Vehicle v1 = new Vehicle();
        v1.setName("Pojazd1");
        v1.setCreateDate(new Date());
        v1.setEditDate(new Date());
        
        session.save(v1);
        
        tx.commit();
        
        Query query = session.createQuery("from Vehicle where id=1");
        Vehicle v = (Vehicle) query.list().get(0);
        
        System.out.println(v.getId() + " " + v.getName() + " " + v.getCreateDate() + " " + v.getEditDate());
            
        tx = session.beginTransaction();
        session.evict(v1);
        v1 = (Vehicle) session.get(Vehicle.class, 1);
        v1.setName("New namexxx");
        session.update(v1);
        tx.commit();
        query = session.createQuery("from Vehicle where id=1");
        v = (Vehicle) query.list().get(0);
        System.out.println(v.getId() + " " + v.getName() + " " + v.getCreateDate() + " " + v.getEditDate());
        instance.close();
    }
}
